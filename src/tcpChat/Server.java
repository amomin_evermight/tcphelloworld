package tcpChat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class Server {

    public static final int DEFAULTPORT = 8082;
    
    public static void main(String[] args) throws IOException {
        
        ServerSocket listener = new ServerSocket(DEFAULTPORT);
        List<ClientConnectionTask> clients = new ArrayList<ClientConnectionTask>();
        BlockingQueue<MessageProtocol> messages = new LinkedBlockingQueue<MessageProtocol>();
        ExecutorService ex = Executors.newCachedThreadPool();
        
        System.out.println("Starting server");
        
        Runnable queueProcessor = new Runnable() {

            @Override
            public void run() {
                try {
                    while(true) {
                        MessageProtocol m = messages.take();
                        for (ClientConnectionTask client : clients) {
                            if (client.isShutdown()) continue;
                            client.send(m);
                        }
                    }
                } catch(InterruptedException iex) {
                    System.err.println("Message processing thread interrupted.");
                }
            }
        };
        ex.execute(queueProcessor);
        
        int count = 0;
        while(!Thread.interrupted()) {
            // try-catch shutdown
            Socket socket = listener.accept();
            ClientConnectionTask c = new ClientConnectionTask(socket, messages);
            c.setName("" + count);
            count++;
            clients.add(c);
            ex.execute(c);
        }
        listener.close();
    }
    
}
