package tcpChat;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Queue;

public class ClientConnectionTask implements Runnable {

    private Socket socket;
    private Queue<MessageProtocol> messages;
    private volatile boolean shutDown;
    private String name;

    // Up to client to make sure the Queue provided is Thread safe
    public ClientConnectionTask(Socket s, Queue<MessageProtocol> messages) {
        socket = s;
        this.messages = messages;
        shutDown = false;
    }    

    public boolean isShutdown() {
        return shutDown;
    }
    
    public void setName(String n) {
        name = n;
    }
    public String getName() {
        return name;
    }
    
    public void send(MessageProtocol tcp) {
        if (socket.isClosed()) return;
        if (isShutdown()) return;
        try {
            tcp.send(socket.getOutputStream());            
        } catch(SocketException brokenPipe) {
            shutDown = true;
            System.err.println("Broken pipe in connection " + getName());
            brokenPipe.printStackTrace(System.out);
        } catch(IOException ioException) {
            System.err.println("IOError sending message in connection " + getName());
            ioException.printStackTrace(System.out);
        }
    }
    
    public void close() {
        shutDown = true;
    }
    
    @Override
    public void run() {
        System.out.println("Opening client connection " + getName());
        MessageProtocol m;
        while(!Thread.interrupted() && !shutDown) {
            try { Thread.sleep(500); } catch(Exception e) {}
            try {
                m = MessageProtocol.read(socket.getInputStream());
                if (m.isDisconnect()) {
                    shutDown = true;
                    MessageProtocol disconnect = MessageProtocol.disconnect();
                    disconnect.send(socket.getOutputStream());
                    break;
                }
                m = new MessageProtocol(getName() + ": " + m.getMessage());
                messages.offer(m);
            } catch (IOException e) {
                m = null;
                e.printStackTrace();
                System.err.println("Error reading from socket in ClientConnection.");
                break;
            }            
        }
        try {
            System.out.println("Closing client connection " + getName());
            shutDown = true;
            socket.close();            
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
