package tcpChat;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Client {

    public static final int DEFAULTPORT = 8082;
    
    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket = new Socket("localhost", DEFAULTPORT);
        ExecutorService ex = Executors.newCachedThreadPool();
        CountDownLatch latch = new CountDownLatch(2);
        InputStream in = System.in;
        
        Runnable read = new Runnable() {
            @Override
            public void run() {
                try {
                    while(!Thread.interrupted()) {
                        String input = Client.readFromStdIn(in);
                        MessageProtocol m = new MessageProtocol(input);
                        // "BYE" will send a disconnect request
                        if (socket.isClosed()) break;
                        m.send(socket.getOutputStream());
                    }                    
                } catch(Exception e) {
                    System.err.println("Error caught in read thread.");
                    e.printStackTrace(System.out);
                }
                latch.countDown();
            }
        };
        Runnable write = new Write(System.in, socket, latch);
        System.out.println("Starting chat");
        ex.execute(write);
        ex.execute(read);
        latch.await();
        System.out.println("Shutting down...");
        ex.shutdown();
        ex.awaitTermination(1000, TimeUnit.SECONDS);
        System.out.println("Chat ended.");
        
    }
    
    private static class Write implements Runnable {
        InputStream in;
        Socket socket;
        CountDownLatch latch;
        
        public Write(InputStream i, Socket s, CountDownLatch l) {
            socket = s;
            in = i;
            latch = l;
        }
                
        @Override
        public void run() {
            try {
                while(!Thread.interrupted()) {
                    MessageProtocol fromServer = MessageProtocol.read(socket.getInputStream());
                    if (fromServer.isDisconnect()) {
                        // Confirmed disconnect.
                        // Need to break out of the read thread too.
                        System.out.println("Disconnecting...");
                        this.in = new ByteArrayInputStream("EXIT".getBytes("UTF-8"));
                        break;
                    }
                    System.out.print(fromServer.getMessage());
                }
                socket.close();
            } catch(IOException e) {
                System.err.println("IO Exception in writing thread...");
                e.printStackTrace(System.out);
            }
            latch.countDown();
        }                    
    }
    public static String readFromStdIn(InputStream in) throws IOException {
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(in));
        return stdIn.readLine();
    }
    
    public static String read(InputStream in) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String stringRequestBody = "";
        String line;
        while ((line = br.readLine()) != null) {
            if (line.equals("END")) break;
            stringRequestBody += line + "%n";
        }
        stringRequestBody = stringRequestBody.replace("%n", System.lineSeparator());
        return stringRequestBody;
    }


}
