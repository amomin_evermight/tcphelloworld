package tcpChat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/** A crappy protocol for exchanging messages
 * 
 * @author user
 *
 */
public class MessageProtocol {

    String message;
    
    public static MessageProtocol disconnect() {
        MessageProtocol disconnect = new MessageProtocol("BYE");
        return disconnect;
    }
    
    public boolean isDisconnect() {
        return message.trim().equals("BYE");
    }
    
    public MessageProtocol(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
        
    public void send(OutputStream out) throws IOException {
        BufferedWriter request = new BufferedWriter(new OutputStreamWriter(out));
        request.write(message);
        request.newLine();
        request.write("END");
        request.newLine();
        request.flush();        
    }
    
    public static MessageProtocol read(InputStream in) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String stringRequestBody = "";
        String line;
        while ((line = br.readLine()) != null) {
            if (line.equals("END")) break;
            stringRequestBody += line + "%n";
        }
        stringRequestBody = stringRequestBody.replace("%n", System.lineSeparator());
        MessageProtocol m = new MessageProtocol(stringRequestBody);
        m.message = stringRequestBody;
        return m;
    }

}
