package tcpTalker;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/** Listens on the specified port, echos back the
 * message sent.  Accepts multiple connections at once.
 * 
 * Export to a jar file.
 * 
 * Run using
 * 
 * <pre>
 * java -jar filename.jar <PORTNUMBER>
 * </pre>
 * 
 * where the port number will be set to 8082 if not specified.
 * 
 * @author user
 *
 */
public class TcpConcurrentEchoListener {
    
    public static final int DEFAULTPORT = 8082;
    public static final int TCPTIMEOUTONEHUNDREDS = 100000;
    public static final int DEFAULTLOGLEVEL = 1;
    
    private ServerSocket listener;
    private int listenPort;
    private boolean shutDown;
    private int logLevel;
    private ExecutorService executorService;
    
    public TcpConcurrentEchoListener(int port, int logLevel) {
        listenPort = port;
        shutDown = false;
        this.logLevel = logLevel;
        executorService = Executors.newCachedThreadPool();
    }
    
    public void open() throws IOException {
        listener = new ServerSocket(listenPort);
        Log("Starting server",3);
    }
        
    public void accept() throws IOException {
        while(!shutDown) {
            Socket socket = listener.accept();
            socket.setSoTimeout(TCPTIMEOUTONEHUNDREDS);
            EchoConnection connection = new EchoConnection(socket, logLevel);
            Callable<Object> task = new Callable<Object>() {
                @Override
                public Object call() throws IOException {
                    connection.start();
                    return null;
                }                
            };
            executorService.submit(task);
        }
    }
        
    private void Log(String message, int level) {
        String stringLogLevel = level<1?"INFO":level<2?"WARN":"ERROR";
        if (level >= logLevel) {
            System.out.printf("Time %10d, %10s: %s%n", System.currentTimeMillis(), stringLogLevel, message);            
        }
    }
    
    public static void main(String[] args) throws IOException {
        
        int port = DEFAULTPORT;
        int logLevel = DEFAULTLOGLEVEL;
        if (args.length > 0) {
            if (args[0].length() > 0 && args[0].replaceAll("\\d", "").length() == 0) {
                port = Integer.parseInt(args[0]);
            }
        }
        if (args.length > 1) {
            if (args[1].length() > 0 && args[1].replaceAll("\\d", "").length() == 0) {
                logLevel = Integer.parseInt(args[1]);
            }
        }
        
        TcpConcurrentEchoListener listener = new TcpConcurrentEchoListener(port, logLevel);
        listener.open();
        try {
            listener.accept();
        } catch (java.net.SocketException socketException) {
            listener.Log(socketException.toString(), 3);
            for (StackTraceElement e : socketException.getStackTrace()) {
                listener.Log(e.toString(), 3);
            }
        }
    }

}
