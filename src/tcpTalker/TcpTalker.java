package tcpTalker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/** Sends 5 messages on a TCP connection (should be sent to a 
 * TcpEchoListener) and prints the conversation.
 * 
 * My gentle introduction to sockets in java.
 * 
 * @author user
 *
 */
public class TcpTalker {

    public static final String DEFAULTHOSTNAME = "localhost";
    public static final int DEFAULTPORT = 8082;
    
    private String hostname;
    private int port;
    private int logLevel;
    private Socket socket;
    private BufferedWriter tcpOut;
    BufferedReader tcpIn;
    
    public TcpTalker(String hostname, int port, int l) {
        this.hostname = hostname;
        this.port = port;
        logLevel = l;
    }
    
    public void open() throws UnknownHostException, IOException {
        socket = new Socket(hostname, port);        
        tcpOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        tcpIn = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
    }
    
    public void close() throws IOException {
        tcpOut.close();
        socket.close();        
    }
    
    public void talk(String message) throws InterruptedException, IOException {
        Log("Sending: " + message, 0);
        tcpOut.write(message);
        tcpOut.newLine();
        tcpOut.write("END");
        tcpOut.newLine();
        tcpOut.flush();
    }
    
    public String listen() throws IOException, InterruptedException {
        String fromSocket = "";
        String line;
        Log("Reading input",0);
        while ((line = tcpIn.readLine()) != null) {
            if (line.contains("END")) {
                break;
            }
            Log(String.format("Read (%d): %s%n", line.length(), line),0);
            fromSocket += line + "%n";
        }
        Log("Done reading lines",0);
        fromSocket = fromSocket.replace("%n", System.lineSeparator());
        return fromSocket;
    }
    
    private void Log(String message, int level) {
        String stringLogLevel = level<1?"INFO":level<2?"WARN":"ERROR";
        if (level >= logLevel) {
            System.out.printf("Time %10d, %10s: %s%n", System.currentTimeMillis(), stringLogLevel, message);
        }
    }
    
    public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
        
        String hostname = DEFAULTHOSTNAME;
        int port = DEFAULTPORT;
        int logLevel = 3;
        
        if (args.length > 1) {
            hostname = args[0];
            port = Integer.parseInt(args[1]);
        }
        if (args.length > 2) {
            logLevel = Integer.parseInt(args[2]);
        }
        
        TcpTalker t = new TcpTalker(hostname, port, logLevel);
        t.open();
        int count = 0;
        while (count < 5) {
            String message = String.format("Request %d", count);
            System.out.println("I said: " + message);
            t.talk(message);
            String response = t.listen();
            if (response.isEmpty()) System.out.println("Didn't hear anything back");                
            else System.out.println("You said: " + response);
            Thread.sleep(1000);
            count++;
        }
        t.talk("BYE");
        t.close();
        System.out.println("Ending...");
    }
}
