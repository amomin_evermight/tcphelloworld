package tcpTalker;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/** Listens on the specified port, echos back the
 * message sent.
 * 
 * Export to a jar file.
 * 
 * Run using
 * 
 * <pre>
 * java -jar filename.jar <PORTNUMBER>
 * </pre>
 * 
 * where the port number will be set to 8082 if not specified.
 * 
 * @author user
 *
 */
public class TcpEchoListener {
    
    public static final int DEFAULTPORT = 8082;
    
    private Socket socket;
    private ServerSocket listener;
    private int listenPort;
    private boolean shutDown;
    private int logLevel;
    
    public TcpEchoListener(int port, int logLevel) {
        listenPort = port;
        shutDown = false;
        this.logLevel = logLevel;
    }
    
    public void open() throws IOException {
        listener = new ServerSocket(listenPort);
        Log("Starting server",3);
    }
    
    public void close() throws IOException {
        Log("Closing server", 3);
        socket.close();
    }
    
    public void accept() throws IOException {
        while(!shutDown) {
            socket = listener.accept();
            //socket.setSoTimeout(10);
            boolean connectionOpen = true;
            Log("Connection accepted", 1);
                while(connectionOpen) {
                    String request = readInputStream(socket.getInputStream());
                    Log(String.format("Received request: %s%n", request), 0);
                    String response = String.format("I heard you say: %n%s", request);
                    if (request.contains("BYE")) {
                        Log("Request to close connection accepted", 1);
                        connectionOpen = false;
                        break;
                    }
                    writeOutput(response, socket.getOutputStream());
                    Log(String.format("Responded with: %s%n", response), 0);
                }                
        }
    }
    
    private String readInputStream(InputStream in) throws IOException {
        Log("Reading input", 0);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String stringRequestBody = "";
        String line;
        while ((line = br.readLine()) != null) {
            if (line.equals("END")) break;
            Log("Reading line (" + line.length() + ") " + line, 0);
            stringRequestBody += line + "%n";
        }
        stringRequestBody = stringRequestBody.replace("%n", System.lineSeparator());
        Log("Read input.", 0);
        return stringRequestBody;
    }
    
    private void writeOutput(String message, OutputStream out) throws IOException {
        Log("Writing to tcpOut.", 0);
        BufferedWriter request = new BufferedWriter(new OutputStreamWriter(out));
        request.write(message);
        request.newLine();
        request.write("END");
        request.newLine();
        request.flush();
        Log(String.format("Wrote %s to output%n", message), 0);
    }
    
    private void Log(String message, int level) {
        String stringLogLevel = level<1?"INFO":level<2?"WARN":"ERROR";
        if (level >= logLevel) {
            System.out.printf("Time %10d, %10s: %s%n", System.currentTimeMillis(), stringLogLevel, message);            
        }
    }
    
    public static void main(String[] args) throws IOException {
        
        int port = DEFAULTPORT;
        int logLevel = 3;
        if (args.length > 0) {
            if (args[0].length() > 0 && args[0].replaceAll("\\d", "").length() == 0) {
                port = Integer.parseInt(args[0]);
            }
        }
        if (args.length > 1) {
            if (args[1].length() > 0 && args[1].replaceAll("\\d", "").length() == 0) {
                logLevel = Integer.parseInt(args[1]);
            }
        }
        
        TcpEchoListener listener = new TcpEchoListener(port, logLevel);
        listener.open();
        try {
            listener.accept();
        } catch (java.net.SocketException socketException) {
            listener.Log(socketException.toString(), 3);
            for (StackTraceElement e : socketException.getStackTrace()) {
                listener.Log(e.toString(), 3);
            }
        }
        listener.close();
    }

}
