package tcpTalker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/** Somewhat similar to type talker, but reads messages from
 * StdIn.
 * 
 * Part of my gentle self-introduction to sockets in java.
 * 
 * @author user
 *
 */
public class TcpTypeTalker {

    public static final String DEFAULTHOSTNAME = "localhost";
    public static final int DEFAULTPORT = 8082;
    
    private String hostname;
    private int port;
    private int logLevel;
    private Socket socket;
    private BufferedWriter tcpOut;
    BufferedReader tcpIn;
    
    public TcpTypeTalker(String hostname, int port, int l) {
        this.hostname = hostname;
        this.port = port;
        logLevel = l;
    }
    
    public void open() throws UnknownHostException, IOException {
        socket = new Socket(hostname, port);        
        tcpOut = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        tcpIn = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
    }
    
    public void close() throws IOException {
        tcpOut.close();
        socket.close();        
    }
    
    public void talk(String message) throws InterruptedException, IOException {
        Log("Sending: " + message, 0);
        tcpOut.write(message);
        tcpOut.newLine();
        tcpOut.write("END");
        tcpOut.newLine();
        tcpOut.flush();
    }
    
    public String readFromStdIn() throws IOException {
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        return stdIn.readLine();
    }
    
    public String listen() throws IOException, InterruptedException {
        String fromSocket = "";
        String line;
        Log("Reading input",0);
        while ((line = tcpIn.readLine()) != null) {
            if (line.contains("END")) {
                break;
            }
            Log(String.format("Read (%d): %s%n", line.length(), line),0);
            fromSocket += line + "%n";
        }
        Log("Done reading lines",0);
        fromSocket = fromSocket.replace("%n", System.lineSeparator());
        return fromSocket;
    }
    
    private void Log(String message, int level) {
        String stringLogLevel = level<1?"INFO":level<2?"WARN":"ERROR";
        if (level >= logLevel) {
            System.out.printf("Time %10d, %10s: %s%n", System.currentTimeMillis(), stringLogLevel, message);
        }
    }
    
    public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
        
        String hostname = DEFAULTHOSTNAME;
        int port = DEFAULTPORT;
        int logLevel = 3;
        
        if (args.length > 1) {
            hostname = args[0];
            port = Integer.parseInt(args[1]);
        }
        if (args.length > 2) {
            logLevel = Integer.parseInt(args[2]);
        }
        
        TcpTypeTalker t = new TcpTypeTalker(hostname, port, logLevel);
        t.open();
        t.talk("Hello");
        System.out.println("Waiting to start session...");
        t.listen();
        System.out.println("Starting session...");
        while (true) {
            System.out.println("Type your message (BYE to exit)");
            String message = t.readFromStdIn();
            if (message.equals("BYE")) {
                break;
            }
            System.out.println("I said: " + message);
            t.talk(message);
            String response = t.listen();
            if (response.isEmpty()) System.out.println("Didn't hear anything back");                
            else System.out.println("Echo server said: " + response);
        }
        t.talk("BYE");
        t.close();
        System.out.println("Ending...");
    }
}
