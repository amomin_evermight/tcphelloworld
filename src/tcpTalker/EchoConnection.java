package tcpTalker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class EchoConnection {
    
    private Socket socket;
    private int logLevel;
    
    public EchoConnection(Socket socket, int logLevel) {
        this.socket = socket;
        this.logLevel = logLevel;
    }
    
    public void start() throws IOException {
        boolean connectionOpen = true;
        Log("Connection accepted", 1);
        while(connectionOpen) {
            String request = readInputStream(socket.getInputStream());
            Log(String.format("Received request: %s%n", request), 0);
            String response = String.format("I heard you say: %n%s", request);
            if (request.contains("BYE")) {
                Log("Request to close connection accepted", 1);
                connectionOpen = false;
                break;
            }
            writeOutput(response, socket.getOutputStream());
            Log(String.format("Responded with: %s%n", response), 0);
        }                                    
        close();
    }
    
    public void close() throws IOException {
        socket.close();
    }

    private String readInputStream(InputStream in) throws IOException {
        Log("Reading input", 0);
        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        String stringRequestBody = "";
        String line;
        while ((line = br.readLine()) != null) {
            if (line.equals("END")) break;
            Log("Reading line (" + line.length() + ") " + line, 0);
            stringRequestBody += line + "%n";
        }
        stringRequestBody = stringRequestBody.replace("%n", System.lineSeparator());
        Log("Read input.", 0);
        return stringRequestBody;
    }
    
    private void writeOutput(String message, OutputStream out) throws IOException {
        Log("Writing to tcpOut.", 0);
        BufferedWriter request = new BufferedWriter(new OutputStreamWriter(out));
        request.write(message);
        request.newLine();
        request.write("END");
        request.newLine();
        request.flush();
        Log(String.format("Wrote %s to output%n", message), 0);
    }

    private void Log(String message, int level) {
        String stringLogLevel = level<1?"INFO":level<2?"WARN":"ERROR";
        if (level >= logLevel) {
            System.out.printf("Time %10d, %10s: %s%n", System.currentTimeMillis(), stringLogLevel, message);            
        }
    }
    
    
}
